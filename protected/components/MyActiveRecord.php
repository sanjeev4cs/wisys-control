<?php

class MyActiveRecord extends CActiveRecord {

	private static $zdb = null;

	protected static function getZDbConnection()
	{
		if (self::$zdb !== null)
			return self::$zdb;
		else
		{
			self::$zdb = Yii::app()->zdb;
			if (self::$zdb instanceof CDbConnection)
			{
				self::$zdb->setActive(true);
				return self::$zdb;
			}
			else
				throw new CDbException(Yii::t('yii','Active Record requires a "db" CDbConnection application component.'));
		}
	}
}
