<?php

/**
 * This is the model class for table "tbl_sensor".
 *
 * The followings are the available columns in table 'tbl_sensor':
 * @property integer $id
 * @property integer $gid
 * @property integer $nid
 * @property integer $stype
 * @property integer $sdata
 * @property string $timestamp
 */
class Sensor extends MyActiveRecord
{
	const SENSOR_PWM = 1;
	const SENSOR_TEMP = 2;
	const SENSOR_HUMIDITY = 4;
	const SENSOR_CO2 = 8;

	public function getDbConnection()
	{
		return self::getZDbConnection();
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_sensor';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('gid, nid, stype, sdata', 'numerical', 'integerOnly'=>true),
			array('timestamp', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, gid, nid, stype, sdata, timestamp', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'gid' => 'Gid',
			'nid' => 'Nid',
			'stype' => 'Stype',
			'sdata' => 'Sdata',
			'timestamp' => 'Timestamp',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('gid',$this->gid);
		$criteria->compare('nid',$this->nid);
		$criteria->compare('stype',$this->stype);
		$criteria->compare('sdata',$this->sdata);
		$criteria->compare('timestamp',$this->timestamp,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function getCurrHumidity($nid)
	{
		$sensor = $this->findBySql("select * from tbl_sensor where stype=4 and nid=$nid order by timestamp desc limit 1;");
		if ($sensor == NULL)
			return "NULL";

		return $sensor->sdata/10;
	}

	public function getCurrTemp($nid)
	{
		$sensor = $this->findBySql("select * from tbl_sensor where stype=2 and nid=$nid order by timestamp desc limit 1;");
		if ($sensor == NULL)
			return "NULL";

		return $sensor->sdata/10;
	}

	public function getCurrPwm($nid)
	{
		$sensor = $this->findBySql("select * from tbl_sensor where stype=1 and nid=$nid order by timestamp desc limit 1;");
		if ($sensor == NULL)
			return "NULL";

		return $sensor->sdata;
	}

	public function getSensorType($type)
	{
		switch($type) {
			case self::SENSOR_PWM:
				return "SENSOR_PWM";
			case self::SENSOR_TEMP:
				return "SENSOR_TEMP";
			case self::SENSOR_HUMIDITY:
				return "SENSOR_HUMIDITY";
			case self::SENSOR_CO2:
				return "SENSOR_CO2";
		}
	}

	public function convertSensorData($sdata)
	{
		return $sdata/10;
	}

	public function FindAllData($gid)
	{
		$criteria=new CDbCriteria;
		$criteria->compare('gid',$this->gid);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

        public function Yearh($nid)
       {
               $sensor = Yii::app()->zdb->createCommand("select nid,sdata,stype,timestamp ,count(*) from tbl_sensor where stype=4 and nid=$nid and  timestamp >= date('now','-360 days')  group by strftime('%s', timestamp);")->queryAll();
               return $sensor;
        }
        public function Yeart($nid)
       {
               $sensor = Yii::app()->zdb->createCommand("select nid,sdata,stype,timestamp from tbl_sensor where stype =2 and  nid=$nid and  timestamp >= date('now','-360 days');")->queryAll();
               return $sensor;
        }
        
       public function Sixh($nid)
       {
               $sensor = Yii::app()->zdb->createCommand("select nid,sdata,stype,timestamp from tbl_sensor where stype=4 and nid=$nid and  timestamp >= date('now','-180 days') order by timestamp desc limit 10   ;")->queryAll();
               return $sensor;
        }
        public function Sixt($nid)
       {
               $sensor = Yii::app()->zdb->createCommand("select nid,sdata,stype,timestamp from tbl_sensor where stype =2 and  nid=$nid and  timestamp >= date('now','-180 days')  order by timestamp desc limit 10 ;")->queryAll();
               return $sensor;
        }
         
         public function Monthh($nid)
       {
               $sensor = Yii::app()->zdb->createCommand("select nid,sdata,stype,timestamp from tbl_sensor where stype=4 and nid=$nid and  timestamp >= date('now','-30 days') order by timestamp desc limit 10  ;")->queryAll();
               return $sensor;
        }
        public function Montht($nid)
       {
               $sensor = Yii::app()->zdb->createCommand("select nid,sdata,stype,timestamp from tbl_sensor where stype =2 and  nid=$nid and  timestamp >= date('now','-30 days') order by timestamp desc limit 10  ;")->queryAll();
               return $sensor;
       }
       
        public function Weektemp($nid)
       {
               $sensor = Yii::app()->zdb->createCommand("select nid,sdata,stype,timestamp from tbl_sensor where nid=$nid and stype=4 and  timestamp >= date('now','-7 days')  ;")->queryAll();
               return $sensor;
        }
         public function Weekhumy($nid)
       {
               $sensor = Yii::app()->zdb->createCommand("select nid,sdata,stype,timestamp from tbl_sensor where nid=$nid and stype=2 and  timestamp >= date('now','-7 days') order by timestamp desc limit 10   ;")->queryAll();
               return $sensor;
        } 
    

        public function Currh($nid)
       {
               $sensor = Yii::app()->zdb->createCommand("select nid,sdata,stype,timestamp from tbl_sensor where stype=4 and nid=$nid and  timestamp >= date('now','-360 days') order by timestamp desc limit 10 ;")->queryAll();
               return $sensor;
        }
        public function Currt($nid)
       {
               $sensor = Yii::app()->zdb->createCommand("select nid,sdata,stype,timestamp from tbl_sensor where stype =2 and  nid=$nid and  timestamp >= date('now','-360 days') order by timestamp desc limit 10 ;")->queryAll();
               return $sensor;
        }

public function Currd($nid)
       {
               $sensor = Yii::app()->zdb->createCommand("select nid,sdata,stype,timestamp from tbl_sensor where stype =1 and  nid=$nid and  timestamp >= date('now','-360 days') order by timestamp desc limit 10 ;")->queryAll();
               return $sensor;
        }

         
        public function Curr($nid)
       {
               $sensor = Yii::app()->zdb->createCommand("select nid,sdata,stype,timestamp from tbl_sensor where  nid=$nid and  timestamp >= date('now','-36 days')   order by  timestamp desc;")->queryAll();
               return $sensor;
        }
         
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Sensor the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
