<?php

/**
 * This is the model class for table "tbl_node".
 *
 * The followings are the available columns in table 'tbl_node':
 * @property integer $nid
 * @property integer $gid
 * @property integer $src_addr
 * @property integer $nw_addr
 * @property string $ieee_addr
 * @property integer $capab
 * @property integer $status
 * @property integer $sensors
 * @property integer $lqi
 * @property integer $pktloss
 */
class Node extends MyActiveRecord
{
	public function getDbConnection()
	{
		return self::getZDbConnection();
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_node';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('gid, src_addr, nw_addr, capab, status, sensors, lqi, pktloss', 'numerical', 'integerOnly'=>true),
			array('ieee_addr', 'length', 'max'=>24),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('nid, gid, src_addr, nw_addr, ieee_addr, capab, status, sensors, lqi, pktloss', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'nid' => 'Nid',
			'gid' => 'Gid',
			'src_addr' => 'Src Addr',
			'nw_addr' => 'Nw Addr',
			'ieee_addr' => 'Ieee Addr',
			'capab' => 'Capab',
			'status' => 'Status',
			'sensors' => 'Sensors',
			'lqi' => 'Lqi',
			'pktloss' => 'Pktloss',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('nid',$this->nid);
		$criteria->compare('gid',$this->gid);
		$criteria->compare('src_addr',$this->src_addr);
		$criteria->compare('nw_addr',$this->nw_addr);
		$criteria->compare('ieee_addr',$this->ieee_addr,true);
		$criteria->compare('capab',$this->capab);
		$criteria->compare('status',$this->status);
		$criteria->compare('sensors',$this->sensors);
		$criteria->compare('lqi',$this->lqi);
		$criteria->compare('pktloss',$this->pktloss);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function getSensorStatus()
	{
		$data = Sensor::model()->getCurrPwm($this->nid);
		if ($data == NULL)
			return -1;

		return $data;
	}

	public function getCurrHumidity()
	{
		$data = Sensor::model()->getCurrHumidity($this->nid);
		return $data;
	}

	public function getCurrTemp()
	{
		$data = Sensor::model()->getCurrTemp($this->nid);
		return $data;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Node the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
