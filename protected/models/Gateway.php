<?php

/**
 * This is the model class for table "tbl_gateway".
 *
 * The followings are the available columns in table 'tbl_gateway':
 * @property integer $id
 * @property string $gid
 * @property string $vid
 * @property integer $node_cnt
 * @property integer $z_alive
 * @property integer $g_alive
 */
class Gateway extends MyActiveRecord
{
	public function getDbConnection()
	{
		return self::getZDbConnection();
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_gateway';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('node_cnt, z_alive, g_alive', 'numerical', 'integerOnly'=>true),
			array('gid', 'length', 'max'=>18),
			array('vid', 'length', 'max'=>12),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, gid, vid, node_cnt, z_alive, g_alive', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'gid' => 'Gid',
			'vid' => 'Vid',
			'node_cnt' => 'Node Cnt',
			'z_alive' => 'Z Alive',
			'g_alive' => 'G Alive',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('gid',$this->gid,true);
		$criteria->compare('vid',$this->vid,true);
		$criteria->compare('node_cnt',$this->node_cnt);
		$criteria->compare('z_alive',$this->z_alive);
		$criteria->compare('g_alive',$this->g_alive);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function searchNodes()
	{
		$criteria=new CDbCriteria;
		$criteria->compare('gid',$this->id);
	//	return new CActiveDataProvider("Node", array(
	//		'criteria'=>$criteria,
	//	));
		return Node::model()->findAll($criteria);
	}

	public function searchSchedules()
	{
		$criteria=new CDbCriteria;
		$criteria->compare('gid',$this->id);
		return Schedule::model()->findAll($criteria);
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Gateway the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
