<?php
/* @var $this SensorController */
/* @var $data Sensor */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gid')); ?>:</b>
	<?php echo CHtml::encode($data->gid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nid')); ?>:</b>
	<?php echo CHtml::encode($data->nid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('stype')); ?>:</b>
	<?php echo CHtml::encode($data->stype); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sdata')); ?>:</b>
	<?php echo CHtml::encode($data->sdata); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('timestamp')); ?>:</b>
	<?php echo CHtml::encode($data->timestamp); ?>
	<br />

</div>
