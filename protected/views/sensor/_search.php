<?php
/* @var $this SensorController */
/* @var $model Sensor */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'gid'); ?>
		<?php echo $form->textField($model,'gid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nid'); ?>
		<?php echo $form->textField($model,'nid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'stype'); ?>
		<?php echo $form->textField($model,'stype'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'sdata'); ?>
		<?php echo $form->textField($model,'sdata'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->