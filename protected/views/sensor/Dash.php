

<?php

echo '<tr><td colspan="4" align="right"><a href="index.php?r=site/dashboardht">Back</a></td></tr>';


if(!empty($user)){
foreach($user as $key=>$val)
{
	
	echo '<tr>';
		//echo '<td>'.$val["nid"].'</td>';
		//echo '<td>'.$val["sdata"].'</td>';
               // $user_array2[] = $val['stype'];
                 //print_r($user_array2);
                
                  
             // The man who invented it doesn't want it. The man who bought it doesn't need it. The man who needs it doesn't know it. What is it
                //print_r($graph_data2);
                if($val['stype']==2)
                 {
                 $user_array[] = $val['sdata']/10;
                 $graph_data ='';  
                if(is_array($user_array))
	        {
		  $graph_data = implode(',',$user_array);
                 
	        }
                 //print_r($graph_data); 
                 } 
                else if ($val['stype']==4)
                  {
                 $user_array2[] = $val['sdata']/10;
                $graph_data2 ='';  
                if(is_array($user_array2))
	        {
		$graph_data2 = implode(',',$user_array2);
	        }
                 //print_r($graph_data2); 
                 } 
                  
                   
                 $user_array1[] = $val['timestamp'];
                $graph_data1 ='';  
                if(is_array($user_array1))
	        {
               
		$graph_data1 = "'".implode("','", $user_array1)."'";
	        }


               
                  
		//echo '<td>'.$val["stype"].'</td>';
                //echo '<td>'.$val["timestamp"].'</td>';
	echo '</tr>';
}

}
else
{
	echo '<tr><td colspan="4">Record not found</td></tr>';
	$graph_data = '';
        $graph_data1 = ''; 
        $graph_data2 = ''; 
}         

?>
</table>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Highcharts Example</title>

        <script type="text/javascript" src="/street-light/assets/fc878f51/jquery.min.js"></script>
        <script type="text/javascript">
    
$(function graph() {
        $('#container').highcharts(
{
            chart: {
                zoomType: 'xy'
            },
            title: {
                text: 'Average Monthly Temperature and Hummidity in cold-storage'
            },
            subtitle: {
                text: 'Source: WiSyTechnolgy.com'
            },
            xAxis: [{
                categories: [<?php echo $graph_data1 ?>  ]
            }],
            yAxis: [{ // Primary yAxis
                labels: {
                    format: '{value}°C',
                    style: {
                        color: '#89A54E'
                    }
                },
                title: {
                    text: 'Temperature',
                    style: {
                        color: '#89A54E'
                    }
                }
            }, { // Secondary yAxis
                title: {
                    text: 'Hummidity',
                    style: {
                        color: '#4572A7'
                    }
                },
                labels: {
                    format: '{value} %RH',
                    style: {
                        color: '#4572A7'
                    }
                },
                opposite: true
            }],
            tooltip: {
                shared: true
            },
            legend: {
                layout: 'vertical',
                align: 'left',
                x: 120,
                verticalAlign: 'top',
                y: 100,
                floating: true,
                backgroundColor: '#FFFFFF'
            },
            series: [{
                name: 'Hummidity',
                threshold : 30, 
                color: 'red',
                type: 'column',
                yAxis: 1,
                data: [<?php echo $graph_data ?>],
                negativeColor: '#4572A7', 
                tooltip: {
                    valueSuffix: ' %RH'
                }
    
            }, {
                name: 'Temperature',
                threshold : 20,
                color: 'red',
                type: 'spline',
                data: [<?php echo $graph_data2 ?>],
                negativeColor: '#89A54E',
                tooltip: {
                    valueSuffix: '°C'
                }
            }]
        });
    });
    
</script>

    </head>
    <body>
<script type="text/javascript" src="/street-light/assets/fc878f51/highcharts.js"></script>
<script type="text/javascript" src="/street-light/assets/fc878f51/exporting.js"></script>


<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
<div id="container1" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

    </body>
</html>
