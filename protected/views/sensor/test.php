
<script src="http://code.highcharts.com/stock/highstock.js"></script>
<script src="http://code.highcharts.com/stock/modules/exporting.js"></script>


<div id="container" style="height: 400px; min-width: 310px"></div>
<div id="container" style="height: 300px"></div>



/**
 * Event handler for applying different colors above and below a threshold value. 
 * Currently this only works in SVG capable browsers. A full solution is scheduled
 * for Highcharts 3.0. In the current example the data is static, so we don't need to
 * recompute after altering the data. In dynamic series, the same event handler 
 * should be added to yAxis.events.setExtremes and possibly other events, like
 * chart.events.resize.
 */

function applyGraphGradient() {
    
    // Options
    var threshold = 25,
        colorAbove = '#EE4643',
        colorBelow = '#4572EE';
        
    // internal
    var someSeries = this.series[0];
    
    $.each(someSeries.points, function(){
        if (this.y < threshold)
        {
           this.graphic.attr('fill', colorBelow);
        }
        else
        {
            this.graphic.attr('fill', colorAbove );
        }
    });
    
    delete someSeries.pointAttr.hover.fill;
    delete someSeries.pointAttr[''].fill; 
}

// Initiate the chart
var chart = new Highcharts.Chart({

    chart: {
        renderTo: 'container',
        events: {
            load: applyGraphGradient
        },
        type:'column'
    },
    
    title: {
        text: 'Average monthly temperature'
    },
    
    yAxis: {
        plotLines: [{
            value: 0,
            color: 'silver',
            width: 2,
            zIndex: 2
        }],
        title: {
            text: 'Temperature (°C)'
        }
    },

    xAxis: {
        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    },

    series: [{
        name: 'Temperature (°C)',
        data: [-2, -3, -2, 2, 5, 9, 11, 11, 10, 8, 4, 30]
    }]

});






/////////////////////////////// Thrshhold Graph ////////////////////////////



<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Highcharts Example</title>

        <script type="text/javascript" src="/street-light/assets/fc878f51/jquery.min.js"></script>
        <script type="text/javascript">


$(function () {
        $('#container').highcharts({
            chart: {
                type: 'line',
                marginRight: 130,
                marginBottom: 25
            },
            title: {
                text: 'Monthly Average Temperature',
                x: -20 //center
            },
            subtitle: {
                text: 'Source: WorldClimate.com',
                x: -20
            },
            xAxis: {
                categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
            },
            yAxis: {
                title: {
                    text: 'Temperature (°C)'
                },
                plotLines: [{
                    
                    color: '#FF0000',
                    dashStyle: 'ShortDash',
                    width: 2,
                    value: 50,
                    zIndex: 0,
                    label : {
                        text : 'Max temperature'
                    }
                }, {
                    color: '#008000',
                    dashStyle: 'ShortDash',
                    width: 2,
                    value: 20,
                    zIndex: 0,
                    label : {
                        text : 'Minmum Temperature '
                    }
                }]
                
            },
            tooltip: {
                valueSuffix: '°C'
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'top',
                x: -10,
                y: 100,
                borderWidth: 0
            },
            series: [{
                name: 'Sensor Data',
                data: [<?php echo $graph_data ?>]
            }]
        });
    });
    
 
</script>

    </head>
    <body>
<script type="text/javascript" src="/street-light/assets/fc878f51/highcharts.js"></script>
<script type="text/javascript" src="/street-light/assets/fc878f51/exporting.js"></script>


<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
<div id="container1" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

    </body>
</html>


