<?php
/* @var $this SensorController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Sensors',
);

$this->menu=array(
	array('label'=>'Manage Sensor', 'url'=>array('admin')),
);
?>

<h1>Sensors</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'dataProvider'=>$dataProvider,
	'columns'=>array(
		'gid',
		'nid',
		'stype',
		'sdata',
		'timestamp'
		),
)); ?>
