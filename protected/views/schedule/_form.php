<?php
/* @var $this ScheduleController */
/* @var $model Schedule */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'schedule-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'type'); ?>
		<?php echo $form->dropDownList($model,'type',array('DIM'=>"SCHEDULE ALL ---  DIM")); ?>
		<?php echo $form->error($model,'type'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'time (hh:mm)'); ?>
		<?php echo $form->textField($model,'time',array('size'=>6,'maxlength'=>6)); ?>
		<?php echo $form->error($model,'time'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Dimming Level'); ?>
		<?php echo $form->textField($model,'pwm',array('size'=>3,'maxlength'=>3)); ?>
		<?php echo $form->error($model,'pwm'); ?>
		<p style="font-size:12px"><font color=red>* </font>Give level as  <b>0</b> for <b>OFF</b> and <b>100</b> for <b>ON</b></p>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
