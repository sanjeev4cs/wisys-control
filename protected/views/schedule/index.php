<?php
/* @var $this ScheduleController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Schedules',
);

$this->menu=array(
	array('label'=>'Create Schedule', 'url'=>array('create')),
	array('label'=>'Manage Schedule', 'url'=>array('admin')),
);
?>

<?php if(Yii::app()->user->hasFlash('message')): ?>
<div class="flash-success">
<?php echo Yii::app()->user->getFlash('message'); ?>
</div>
<?php endif;

Yii::app()->clientScript->registerScript(
		'errHideEffect',
		'$(".flash-success").animate({opacity: 1.0}, 5000).fadeOut("slow");',
		CClientScript::POS_READY
		);
?>

<h1>Schedules</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'dataProvider'=>$dataProvider,
	'columns'=>array(
		'type',
		'time',
		'pwm',
		'status'
		),
)); ?>

<?php echo CHtml::beginForm(array('Node/command'), 'get'); ?>
<div class="row buttons">
	<?php echo CHtml::submitButton('Activate', array('name'=>"schedule", 'style'=>"float: right")); ?>
</div>
<?php echo CHtml::endForm(); ?>
