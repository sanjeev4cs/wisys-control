<?php
/* @var $this NodeController */
/* @var $model Node */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'nid'); ?>
		<?php echo $form->textField($model,'nid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'gid'); ?>
		<?php echo $form->textField($model,'gid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'src_addr'); ?>
		<?php echo $form->textField($model,'src_addr'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nw_addr'); ?>
		<?php echo $form->textField($model,'nw_addr'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ieee_addr'); ?>
		<?php echo $form->textField($model,'ieee_addr'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'capab'); ?>
		<?php echo $form->textField($model,'capab'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'status'); ?>
		<?php echo $form->textField($model,'status'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'sensors'); ?>
		<?php echo $form->textField($model,'sensors'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->