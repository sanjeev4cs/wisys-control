<?php
/* @var $this NodeController */
/* @var $model Node */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'node-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'gid'); ?>
		<?php echo $form->textField($model,'gid'); ?>
		<?php echo $form->error($model,'gid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'src_addr'); ?>
		<?php echo $form->textField($model,'src_addr'); ?>
		<?php echo $form->error($model,'src_addr'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nw_addr'); ?>
		<?php echo $form->textField($model,'nw_addr'); ?>
		<?php echo $form->error($model,'nw_addr'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ieee_addr'); ?>
		<?php echo $form->textField($model,'ieee_addr'); ?>
		<?php echo $form->error($model,'ieee_addr'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'capab'); ?>
		<?php echo $form->textField($model,'capab'); ?>
		<?php echo $form->error($model,'capab'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->textField($model,'status'); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sensors'); ?>
		<?php echo $form->textField($model,'sensors'); ?>
		<?php echo $form->error($model,'sensors'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->