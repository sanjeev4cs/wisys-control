<?php
/* @var $this NodeController */
/* @var $data Node */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('nid')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->nid), array('view', 'id'=>$data->nid)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gid')); ?>:</b>
	<?php echo CHtml::encode($data->gid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('src_addr')); ?>:</b>
	<?php echo CHtml::encode($data->src_addr); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nw_addr')); ?>:</b>
	<?php echo CHtml::encode($data->nw_addr); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ieee_addr')); ?>:</b>
	<?php echo CHtml::encode($data->ieee_addr); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('capab')); ?>:</b>
	<?php echo CHtml::encode($data->capab); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('sensors')); ?>:</b>
	<?php echo CHtml::encode($data->sensors); ?>
	<br />

	*/ ?>

</div>