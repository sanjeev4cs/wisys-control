<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>

<div class="container" id="page">

	<div id="header">
		<?php echo CHtml::image(Yii::app()->request->baseUrl.'/images/logo-small.png', 'Logo', array('height'=>88, 'width'=>102, 'style'=>"float: right")); ?>
		<div id="logo"><?php echo CHtml::encode(Yii::app()->name); ?></div>
	</div><!-- header -->

	<div id="mainmenu">
		<?php $this->widget('zii.widgets.CMenu',array(
			'items'=>array(
				array('label'=>'Dashboard-EZLux', 'url'=>array('/site/dashboard'), 'visible'=>!Yii::app()->user->isGuest && Yii::app()->user->isEZLux() && !Yii::app()->user->isAdmin()),
				array('label'=>'Dashboard-EZCold', 'url'=>array('/site/dashboardht'), 'visible'=>!Yii::app()->user->isGuest && Yii::app()->user->isEZCold()),
				array('label'=>'Dashboard-Admin', 'url'=>array('/site/admindash'), 'visible'=>!Yii::app()->user->isGuest && Yii::app()->user->isAdmin()),
				array('label'=>'Home', 'url'=>array('/site/index'), 'visible'=>Yii::app()->user->isGuest),
				array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
				array('label'=>'Schedule', 'url'=>array('schedule/index'), 'visible'=>!Yii::app()->user->isGuest && Yii::app()->user->isEZLux() && !Yii::app()->user->isAdmin()),
				array('label'=>'Users', 'url'=>array('user/admin'), 'visible'=>!Yii::app()->user->isGuest && Yii::app()->user->isAdmin()),
				array('label'=>'Reports', 'url'=>array('/site/reports'), 'visible'=>!Yii::app()->user->isGuest && Yii::app()->user->isEZCold()),
				array('label'=>'Settings', 'url'=>array('/site/settings'), 'visible'=>!Yii::app()->user->isGuest && Yii::app()->user->isEZCold()),
				//array('label'=>'About', 'url'=>array('/site/page', 'view'=>'about')),
				//array('label'=>'Contact', 'url'=>array('/site/contact')),
				array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
			),
		)); ?>
	</div><!-- mainmenu -->
	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>

	<?php echo $content; ?>

	<div class="clear"></div>

	<div id="footer">
		Copyright &copy; 2014 Wisys Technologies.<br/>
		All Rights Reserved.<br/>
	</div><!-- footer -->

</div><!-- page -->

</body>
</html>
