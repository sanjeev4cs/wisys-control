<?php

echo '<tr><td colspan="4" align="right"><a href="index.php?r=site/reports">Back</a></td></tr>';


if(!empty($user)){
foreach($user as $key=>$val)
{
	
	echo '<tr>';
		//echo '<td>'.$val["nid"].'</td>';
		//echo '<td>'.$val["sdata"].'</td>';
                $user_array[] = $val['sdata']/10;
                $graph_data ='';  
                if(is_array($user_array))
	        {
		$graph_data = implode(',',$user_array);
	        }   
           
                 $user_array1[] = $val['timestamp'];
                $graph_data1 ='';  
                if(is_array($user_array1))
	        {
               
		$graph_data1 = "'".implode("','", $user_array1)."'";
	        } 
                 //print_r($graph_data1);
		//echo '<td>'.$val["stype"].'</td>';
                //echo '<td>'.$val["timestamp"].'</td>';
	echo '</tr>';
}

}
else
{
	echo '<tr><td colspan="4">Record not found</td></tr>';
	$graph_data = '';
}

?>

<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Highcharts Example</title>

        <script type="text/javascript" src="/street-light/assets/fc878f51/jquery.min.js"></script>
        <script type="text/javascript">
    
$(function graph() {
        $('#container').highcharts({
            chart: {
                zoomType: 'xy'
            },
            title: {
                text: 'Average Sensor data  in cold-storage'
            },
            subtitle: {
                text: 'Source: WiSySTechnolgy.com'
            },
            xAxis: [{
                categories: [<?php echo $graph_data1 ?> ]
            }],
            yAxis: [{ // Primary yAxis
                labels: {
                    format: '{value}°C + %RH',
                    style: {
                        color: '#89A54E'
                    }
                },
                title: {
                    text: 'Sensor Data',
                    style: {
                        color: '#89A54E'
                    }
                }
            }, ],
            tooltip: {
                shared: true
            },
            legend: {
                layout: 'vertical',
                align: 'left',
                x: 120,
                verticalAlign: 'top',
                y: 100,
                floating: true,
                backgroundColor: '#FFFFFF'
            },
            series: [
    
            {
                name: 'Sensor Data',
                color: '#89A54E',
                type: 'spline',
                data: [<?php echo $graph_data ?>],
                tooltip: {
                    valueSuffix: '°C + %RH'
                }
            }]
        });
    });
    
</script>

    </head>
    <body>
<script type="text/javascript" src="/street-light/assets/fc878f51/highcharts.js"></script>
<script type="text/javascript" src="/street-light/assets/fc878f51/exporting.js"></script>


<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
<div id="container1" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

    </body>
</html>

