<?php
/* @var $this SiteController */

$this->pageTitle='Wisys Admin GW Test';
$this->breadcrumbs=array(
	'GW Test - ' . $gw->gid,
);
?>

<?php if(Yii::app()->user->hasFlash('message')): ?>
<div class="flash-success">
<?php echo Yii::app()->user->getFlash('message'); ?>
</div>
<?php endif;

Yii::app()->clientScript->registerScript(
		'errHideEffect',
		'$(".flash-success").animate({opacity: 1.0}, 5000).fadeOut("slow");',
		CClientScript::POS_READY
		);
?>

<?php
	echo CHtml::beginForm(array('Node/Test'), 'get');
	echo "<table border=\"1\" align=\"center\">";
	echo "<tr bgcolor=\"#9cd54a\" style=\"color:white;\">";
	echo "<td><b>Node ID</td></b>";
	echo "<td><b>Node Address</b></td>";
	echo "<td><b>Node Status</b></td>";
	echo "<td><b>Test Node</b></td>";
	echo "<td><b>Test Result</b></td>";
	echo "</tr>";
	$nodes = $gw->SearchNodes();
	foreach($nodes as $node) {
		echo "<tr>";
		echo "<td>"; echo CHtml::encode($node->nid); echo "</td>";
		echo "<td>"; echo CHtml::encode($node->ieee_addr); echo "</td>";
		echo "<td>";
		if ($node->status == 1)
			echo CHtml::image(Yii::app()->request->baseUrl.'/images/grn_round.png', 'Link Status', array('height'=>20, 'width'=>20));
		else
			echo CHtml::image(Yii::app()->request->baseUrl.'/images/grey_round.png', 'Link Status', array('height'=>20, 'width'=>20));
		echo "</td>";

		echo "<td>";
		echo CHtml::submitButton('Test', array('name'=>"$node->nid", 'style'=>"float: center"));
		echo "<input type='hidden' name='n_id' value='$node->nid' />";
		echo "</td>";
	}

	echo "</tr>";
	echo "</table>";
	echo CHtml::endForm();
?>
