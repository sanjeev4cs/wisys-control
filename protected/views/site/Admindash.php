<?php
/* @var $this SiteController */

$this->pageTitle='Wisys Admin Dashboard';
$this->breadcrumbs=array(
	'Dashboard',
);
?>

<?php if(Yii::app()->user->hasFlash('message')): ?>
<div class="flash-success">
<?php echo Yii::app()->user->getFlash('message'); ?>
</div>
<?php endif;

Yii::app()->clientScript->registerScript(
		'errHideEffect',
		'$(".flash-success").animate({opacity: 1.0}, 5000).fadeOut("slow");',
		CClientScript::POS_READY
		);
?>

<h2><i>Welcome to your Dashboard</i></h1>
<?php
	echo CHtml::beginForm(array('Gateway/Test'), 'get');
	echo "<table border=\"1\" align=\"center\">";
	echo "<tr bgcolor=\"#9cd54a\" style=\"color:white;\">";
	echo "<td><b>Gateway ID</td></b>";
	echo "<td><b>Node Count</b></td>";
	echo "<td><b>Link Status</b></td>";
	echo "<td><b>Gateway Status</b></td>";
	echo "<td><b>Test</b></td>";
	echo "</tr>";
	foreach($gwlist as $gw) {
		echo "<tr>";
		echo "<td>"; echo CHtml::encode($gw->gid); echo "</td>";
		echo "<td>"; echo CHtml::encode($gw->node_cnt); echo "</td>";
		echo "<td>";
		if ($gw->g_alive == 1)
			echo CHtml::image(Yii::app()->request->baseUrl.'/images/grn_round.png', 'Link Status', array('height'=>20, 'width'=>20));
		else
			echo CHtml::image(Yii::app()->request->baseUrl.'/images/grey_round.png', 'Link Status', array('height'=>20, 'width'=>20));
		echo "</td>";

		echo "<td>";
		if ($gw->z_alive == 1)
			echo CHtml::image(Yii::app()->request->baseUrl.'/images/grn_round.png', 'Link Status', array('height'=>20, 'width'=>20));
		else
			echo CHtml::image(Yii::app()->request->baseUrl.'/images/grey_round.png', 'Link Status', array('height'=>20, 'width'=>20));
		echo "</td>";
		echo "<td>";
		echo CHtml::submitButton('Test', array('name'=>"$gw->gid", 'style'=>"float: center"));
		echo "<input type='hidden' name='g_id' value='$gw->gid' />";
		echo "</td>";
	}

	echo "</tr>";
	echo "</table>";
	echo CHtml::endForm();
?>
