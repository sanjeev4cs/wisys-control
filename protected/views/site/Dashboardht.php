<?php
/* @var $this SiteController */

$this->pageTitle='Wisys ' . Yii::app()->user->appName() . ' - Dashboard';
$this->breadcrumbs=array(
	'Dashboard-EZCold',
);
?>

<body onload="JavaScript:AutoRefresh(60000);">
<script type="text/javascript">
<!--
function AutoRefresh(t) {
	setTimeout("location.reload(true);", t);
}
// -->
</script>

<?php if(Yii::app()->user->hasFlash('message')): ?>
<div class="flash-success">
<?php echo Yii::app()->user->getFlash('message'); ?>
</div>
<?php endif;

Yii::app()->clientScript->registerScript(
		'errHideEffect',
		'$(".flash-success").animate({opacity: 1.0}, 5000).fadeOut("slow");',
		CClientScript::POS_READY
		);
?>

<h2><i>Welcome to your Dashboard</i></h1>
<?php echo CHtml::beginForm(array('Node/command'), 'get'); ?>
<table border="1" align="center">
<tr bgcolor="#9cd54a" style="color:white;">
<td><b>Gateway ID</td></b>
<td><b>Gateway Location</b></td>
<td><b>Node Count</b></td>
<td><b>Link Status</b></td>
<td><b>Gateway Status</b></td>
</tr>
<tr>
<td><?php echo CHtml::encode($gw->gid); ?></td>
<td><?php echo CHtml::encode($user->location); ?></td>
<td><?php echo CHtml::encode($gw->node_cnt); ?></td>
<?php
	echo "<td>";
	if ($gw->g_alive == 1)
		echo CHtml::image(Yii::app()->request->baseUrl.'/images/grn_round.png', 'Link Status', array('height'=>20, 'width'=>20));
	else
		echo CHtml::image(Yii::app()->request->baseUrl.'/images/grey_round.png', 'Link Status', array('height'=>20, 'width'=>20));
	echo "</td>";

	echo "<td>";
	if ($gw->z_alive == 1)
		echo CHtml::image(Yii::app()->request->baseUrl.'/images/grn_round.png', 'Link Status', array('height'=>20, 'width'=>20));
	else
		echo CHtml::image(Yii::app()->request->baseUrl.'/images/grey_round.png', 'Link Status', array('height'=>20, 'width'=>20));
	echo "</td>";
?>
</tr>
</table>
<h2><i>Registered Nodes</i></h2>
<script type="text/JavaScript">
<!-- Begin
function popjack()
{
	window.open('STLC_Wisys.pdf','popjack','toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,copyhistory=no,scrollbars=yes,width=480,height=320');
}
//-->
</script>
<a href="JavaScript: popjack()" onMouseOver="window.status='Status Bar Message'; return true" onMouseOut="window.status=''; return true">Help</a>
<br><br>

<script type="text/JavaScript">
function toggle(source)
{
	var aInputs = document.getElementsByTagName('input');

	for (var i=0;i<aInputs.length;i++) {
		if (aInputs[i] != source && aInputs[i].className == source.className) {
			aInputs[i].checked = source.checked;
		}
	}
}

function uncheck()
{
	document.getElementById('chall').checked = false;
}
</script>

<table border="1" align="center">
<tr bgcolor="#9cd54a" style="color:white;">
<td><b>Node ID</td></b>
<td><b>IEEE Address</b></td>
<td><b>Link Status</b></td>
<td><b>RSSI</b></td>
<td><b>% Drop</b></td>
<td><b>Humidity</b></td>
<td><b>Temperature</b></td>
</tr>
<?php
$nodes = $gw->searchNodes();
foreach($nodes as $node) {
	echo "<tr>";
	echo "<td>";echo $node->nid;echo"</td>";
	echo "<td>";echo $node->ieee_addr;echo"</td>";
	echo "<td>";
	if ($node->status == 1)
		echo CHtml::image(Yii::app()->request->baseUrl.'/images/grn_round.png', 'Link Status', array('height'=>20, 'width'=>20));
	else
		echo CHtml::image(Yii::app()->request->baseUrl.'/images/grey_round.png', 'Link Status', array('height'=>20, 'width'=>20));
	echo "</td>";
	echo "<td>";
	if ($node->lqi == 0)
		echo CHtml::image(Yii::app()->request->baseUrl.'/images/0.png', 'Link Status', array('height'=>20, 'width'=>20));
	else if ($node->lqi <= 30)
		echo CHtml::image(Yii::app()->request->baseUrl.'/images/1.png', 'Link Status', array('height'=>20, 'width'=>20));
	else if ($node->lqi <= 70)
		echo CHtml::image(Yii::app()->request->baseUrl.'/images/2.png', 'Link Status', array('height'=>20, 'width'=>20));
	else if ($node->lqi <= 130)
		echo CHtml::image(Yii::app()->request->baseUrl.'/images/3.png', 'Link Status', array('height'=>20, 'width'=>20));
	else if ($node->lqi <= 200)
		echo CHtml::image(Yii::app()->request->baseUrl.'/images/4.png', 'Link Status', array('height'=>20, 'width'=>20));
	else
		echo CHtml::image(Yii::app()->request->baseUrl.'/images/5.png', 'Link Status', array('height'=>20, 'width'=>20));
	echo"</td>";
	echo "<td>";echo $node->pktloss;echo"</td>";
	echo "<td>";echo $node->getCurrHumidity();echo"</td>";
	echo "<td>";echo $node->getCurrTemp();echo"</td>";
	echo "</tr>";
}

//$this->widget('zii.widgets.grid.CGridView', array('dataProvider'=>$nodes,));

?>
</table>
<?php echo CHtml::endForm(); ?>



<?php

echo CHtml::beginForm(array('sensor/reqTest02'), 'get');

	
	$nodes = $gw->searchNodes();
	$array = array();
	foreach($nodes as $node)
		$array = $array + array("$node->nid"=>"$node->nid");

	echo Chtml::dropDownList("Nodelist", "ALL", $array);

	?>



      
        <?php   echo CHtml::dropDownList('Sensor', 'selected value', array('H'=>'Hummidity', 'T'=>'Temperature'));?> 

	<?php echo CHtml::submitButton('Node Graph', array('name'=>"apply")); 

       echo CHtml::endForm();

?>

<?php

echo CHtml::beginForm(array('sensor/test'), 'get');

	
	$nodes = $gw->searchNodes();
	$array = array();
	foreach($nodes as $node)
		$array = $array + array("$node->nid"=>"$node->nid");

	echo Chtml::dropDownList("Nodelist", "ALL", $array);

	?>



      
        

	<?php echo CHtml::submitButton('Node Graph', array('name'=>"apply")); 

       echo CHtml::endForm();

?>



