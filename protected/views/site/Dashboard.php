<?php
/* @var $this SiteController */

$this->pageTitle='Wisys ' . Yii::app()->user->appName() . ' - Dashboard';
$this->breadcrumbs=array(
	'Dashboard',
);
?>

<body onload="JavaScript:AutoRefresh(60000);">
<script type="text/javascript">
<!--
function AutoRefresh(t) {
	setTimeout("location.reload(true);", t);
}
// -->
</script>

<?php if(Yii::app()->user->hasFlash('message')): ?>
<div class="flash-success">
<?php echo Yii::app()->user->getFlash('message'); ?>
</div>
<?php endif;

Yii::app()->clientScript->registerScript(
		'errHideEffect',
		'$(".flash-success").animate({opacity: 1.0}, 5000).fadeOut("slow");',
		CClientScript::POS_READY
		);
?>

<h2><i>Welcome to your Dashboard</i></h1>
<?php echo CHtml::beginForm(array('Node/command'), 'get'); ?>
<table border="1" align="center">
<tr bgcolor="#9cd54a" style="color:white;">
<td><b>Gateway ID</td></b>
<td><b>Gateway Location</b></td>
<td><b>Node Count</b></td>
<td><b>Link Status</b></td>
<td><b>Gateway Status</b></td>
</tr>
<tr>
<td><?php echo CHtml::encode($gw->gid); ?></td>
<td><?php echo CHtml::encode($user->location); ?></td>
<td><?php echo CHtml::encode($gw->node_cnt); ?></td>
<?php
	echo "<td>";
	if ($gw->g_alive == 1)
		echo CHtml::image(Yii::app()->request->baseUrl.'/images/grn_round.png', 'Link Status', array('height'=>20, 'width'=>20));
	else
		echo CHtml::image(Yii::app()->request->baseUrl.'/images/grey_round.png', 'Link Status', array('height'=>20, 'width'=>20));
	echo "</td>";

	echo "<td>";
	if ($gw->z_alive == 1)
		echo CHtml::image(Yii::app()->request->baseUrl.'/images/grn_round.png', 'Link Status', array('height'=>20, 'width'=>20));
	else
		echo CHtml::image(Yii::app()->request->baseUrl.'/images/grey_round.png', 'Link Status', array('height'=>20, 'width'=>20));
	echo "</td>";
?>
</tr>
</table>
<?php
$color = "green";
$status = "None";
$cmd = Command::model()->find('(gid)=?', array($gw->id));
if ($cmd) {
	if ($cmd->scode == 2)
		$status = "In Progress";
	else if($cmd->scode == 0)
		$status = "Success";
	else {
		$status = "Pending";
		$color = "red";
	}
}
?>
<?php echo CHtml::Label($status, false, array('style'=>"float: right; color: $color")); ?>
<?php echo CHtml::Label("Recent Command Status: ", false, array('style'=>"float: right")); ?>
<h2><i>Registered Lights</i></h2>
<script type="text/JavaScript">
<!-- Begin
function popjack()
{
	window.open('STLC_Wisys.pdf','popjack','toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,copyhistory=no,scrollbars=yes,width=480,height=320');
}
//-->
</script>
<a href="JavaScript: popjack()" onMouseOver="window.status='Status Bar Message'; return true" onMouseOut="window.status=''; return true">Help</a>
<?php echo CHtml::submitButton('Alert', array('name'=>"onoff", 'style'=>"float: right")); ?>
<?php echo CHtml::submitButton('Dim', array('name'=>"dim", 'style'=>"float: right")); ?>
<?php echo CHtml::submitButton('Reset', array('name'=>"reset", 'style'=>"float: right")); ?>
<br><br>

<script type="text/JavaScript">
function toggle(source)
{
	var aInputs = document.getElementsByTagName('input');

	for (var i=0;i<aInputs.length;i++) {
		if (aInputs[i] != source && aInputs[i].className == source.className) {
			aInputs[i].checked = source.checked;
		}
	}
}

function uncheck()
{
	document.getElementById('chall').checked = false;
}
</script>

<table border="1" align="center">
<tr bgcolor="#9cd54a" style="color:white;">
<td><input type='hidden' name='chall' value='off' />
<input type='checkbox' id='chall' name='chall' class='selectnode' onClick='toggle(this)' /></td>
<td><b>Node ID</td></b>
<td><b>IEEE Address</b></td>
<td><b>Link Status</b></td>
<td><b>Light Status</b></td>
<td><b>RSSI</b></td>
<td><b>% Drop</b></td>
<?php echo "<td>";echo CHtml::rangeField("dim_all",0, array('onchange'=>"document.getElementById('rval').innerHTML = this.value;")); echo CHtml::label("0", "false", array('id'=>'rval')); echo "</td>"; ?>
</tr>
<?php
$nodes = $gw->searchNodes();
foreach($nodes as $node) {
	echo "<tr>";
	echo "<td><input type='hidden' name='c_";echo $node->nid;echo"' value='off' />";
	echo "<input name='c_";echo $node->nid; echo"' type='checkbox' class='selectnode' onclick='uncheck()'></td>";
	echo "<td>";echo $node->nid;echo"</td>";
	echo "<td>";echo $node->ieee_addr;echo"</td>";
	echo "<td>";
	if ($node->status == 1)
		echo CHtml::image(Yii::app()->request->baseUrl.'/images/grn_round.png', 'Link Status', array('height'=>20, 'width'=>20));
	else
		echo CHtml::image(Yii::app()->request->baseUrl.'/images/grey_round.png', 'Link Status', array('height'=>20, 'width'=>20));
	echo "</td>";
	echo "<td>";
	$sdata = $node->getSensorStatus();
	$adata = $sdata;
	if ($sdata > 100) {
		echo CHtml::image(Yii::app()->request->baseUrl.'/images/red_round.png', 'Link Status', array('height'=>20, 'width'=>20));
		$adata = 100;
	}
	else if ($sdata > 0)
		echo CHtml::image(Yii::app()->request->baseUrl.'/images/grn_round.png', 'Link Status', array('height'=>20, 'width'=>20));
	else
		echo CHtml::image(Yii::app()->request->baseUrl.'/images/grey_round.png', 'Link Status', array('height'=>20, 'width'=>20));
	echo "</td>";
	echo "<td>";
	if ($node->lqi == 0)
		echo CHtml::image(Yii::app()->request->baseUrl.'/images/0.png', 'Link Status', array('height'=>20, 'width'=>20));
	else if ($node->lqi <= 30)
		echo CHtml::image(Yii::app()->request->baseUrl.'/images/1.png', 'Link Status', array('height'=>20, 'width'=>20));
	else if ($node->lqi <= 70)
		echo CHtml::image(Yii::app()->request->baseUrl.'/images/2.png', 'Link Status', array('height'=>20, 'width'=>20));
	else if ($node->lqi <= 130)
		echo CHtml::image(Yii::app()->request->baseUrl.'/images/3.png', 'Link Status', array('height'=>20, 'width'=>20));
	else if ($node->lqi <= 200)
		echo CHtml::image(Yii::app()->request->baseUrl.'/images/4.png', 'Link Status', array('height'=>20, 'width'=>20));
	else
		echo CHtml::image(Yii::app()->request->baseUrl.'/images/5.png', 'Link Status', array('height'=>20, 'width'=>20));
	echo"</td>";
	echo "<td>";echo $node->pktloss;echo"</td>";
	echo "<td>";echo CHtml::rangeField("r_$node->nid",$sdata, array('onchange'=>"document.getElementById('l_$node->nid').innerHTML = this.value;")); echo CHtml::label($adata, "false", array("id"=>"l_$node->nid"));echo "</td>";
	echo "</tr>";
}

//$this->widget('zii.widgets.grid.CGridView', array('dataProvider'=>$nodes,));

?>
</table>
<?php echo CHtml::submitButton('Alert', array('name'=>"onoff", 'style'=>"float: right")); ?>
<?php echo CHtml::submitButton('Dim', array('name'=>"dim", 'style'=>"float: right")); ?>
<?php echo CHtml::submitButton('Reset', array('name'=>"reset", 'style'=>"float: right")); ?>
<?php echo CHtml::endForm(); ?>
