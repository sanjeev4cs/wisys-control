<?php
/* @var $this SiteController */
$this->pageTitle='Wisys ' . Yii::app()->user->appName() . ' Settings';
?>
<?php echo CHtml::beginForm(array('Node/setting'), 'get'); ?>

<div class="row">
	<?php echo CHtml::label("Reporting Interval(s): ", false); ?>
	<?php
	$nodes = $gw->searchNodes();
	$array = array("ALL"=>"All Nodes");
	foreach($nodes as $node)
		$array = $array + array("$node->nid"=>"$node->nid");

	echo Chtml::dropDownList("Nodelist", "ALL", $array);
	?>
	<?php echo CHtml::textField("interval", "", array('size'=>3,'maxlength'=>3)); ?>
	<?php echo CHtml::submitButton('Apply', array('name'=>"apply")); ?>
</div>

<?php echo CHtml::endForm(); ?>

