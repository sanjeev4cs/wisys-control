<?php
/* @var $this SiteController */

$this->pageTitle='Wisys ' . Yii::app()->user->appName() . ' Report';



$fulldata = Sensor::model()->FindAllData($gw->id);
if ($fulldata != NULL)
	$this->widget('zii.widgets.grid.CGridView', array(
		'dataProvider'=>$fulldata,
		'columns'=>array(
			'nid',
			'timestamp',
			array(
				'name'=>'stype',
				'value'=>'$data->getSensorType($data->stype)',
			     ),
			array(
				'name'=>'sdata',
				'value'=>'$data->convertSensorData($data->sdata)',
			     ),
			),
	));
else
	echo "<br><br> No Data Found";

?>

<?php  
    echo CHtml::beginForm(array('Sensor/duration'), 'get');

	
	$nodes = $gw->searchNodes();
	$array = array();
	foreach($nodes as $node)
		$array = $array + array("$node->nid"=>"$node->nid");

	echo Chtml::dropDownList("Nodelist", "ALL", $array);
	?>
       
      <?php   echo CHtml::dropDownList('input name', 'selected value', CMap::mergeArray(array('M'=>'one Month', 'W'=>'one Week'), array('S'=>'Six Months', 'O'=>'One Year')));?>
      <?php   echo CHtml::dropDownList('Sensor', 'selected value', array('H'=>'Hummidity', 'T'=>'Temperature'));?>  
	<?php echo CHtml::submitButton('Apply', array('name'=>"apply")); 

echo CHtml::endForm();

?>
