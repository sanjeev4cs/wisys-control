<?php
/* @var $this GatewayController */
/* @var $model Gateway */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'gid'); ?>
		<?php echo $form->textField($model,'gid',array('size'=>18,'maxlength'=>18)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'vid'); ?>
		<?php echo $form->textField($model,'vid',array('size'=>12,'maxlength'=>12)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'node_cnt'); ?>
		<?php echo $form->textField($model,'node_cnt'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'z_alive'); ?>
		<?php echo $form->textField($model,'z_alive'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'g_alive'); ?>
		<?php echo $form->textField($model,'g_alive'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->