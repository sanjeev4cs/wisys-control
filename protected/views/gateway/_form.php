<?php
/* @var $this GatewayController */
/* @var $model Gateway */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gateway-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'gid'); ?>
		<?php echo $form->textField($model,'gid',array('size'=>18,'maxlength'=>18)); ?>
		<?php echo $form->error($model,'gid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'vid'); ?>
		<?php echo $form->textField($model,'vid',array('size'=>12,'maxlength'=>12)); ?>
		<?php echo $form->error($model,'vid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'node_cnt'); ?>
		<?php echo $form->textField($model,'node_cnt'); ?>
		<?php echo $form->error($model,'node_cnt'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'z_alive'); ?>
		<?php echo $form->textField($model,'z_alive'); ?>
		<?php echo $form->error($model,'z_alive'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'g_alive'); ?>
		<?php echo $form->textField($model,'g_alive'); ?>
		<?php echo $form->error($model,'g_alive'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->