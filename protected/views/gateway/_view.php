<?php
/* @var $this GatewayController */
/* @var $data Gateway */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gid')); ?>:</b>
	<?php echo CHtml::encode($data->gid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vid')); ?>:</b>
	<?php echo CHtml::encode($data->vid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('node_cnt')); ?>:</b>
	<?php echo CHtml::encode($data->node_cnt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('z_alive')); ?>:</b>
	<?php echo CHtml::encode($data->z_alive); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('g_alive')); ?>:</b>
	<?php echo CHtml::encode($data->g_alive); ?>
	<br />


</div>