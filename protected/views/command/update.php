<?php
/* @var $this CommandController */
/* @var $model Command */

$this->breadcrumbs=array(
	'Commands'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Command', 'url'=>array('index')),
	array('label'=>'Create Command', 'url'=>array('create')),
	array('label'=>'View Command', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Command', 'url'=>array('admin')),
);
?>

<h1>Update Command <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>