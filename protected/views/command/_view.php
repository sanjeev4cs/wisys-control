<?php
/* @var $this CommandController */
/* @var $data Command */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gid')); ?>:</b>
	<?php echo CHtml::encode($data->gid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('command')); ?>:</b>
	<?php echo CHtml::encode($data->command); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('params')); ?>:</b>
	<?php echo CHtml::encode($data->params); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('scode')); ?>:</b>
	<?php echo CHtml::encode($data->scode); ?>
	<br />


</div>