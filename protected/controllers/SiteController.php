<?php

class SiteController extends MyController
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->render('index');
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login()) {
				if (strcmp($model->username, "admin") === 0) {
					Yii::app()->params['isAdmin'] = true;
					$this->redirect('index.php?r=user/admin');
				}
				else if (Yii::app()->user->isEZLux())
					$this->redirect('index.php?r=site/dashboard');
				else if (Yii::app()->user->isEZCold())
					$this->redirect('index.php?r=site/dashboardht');
			}
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect('index.php?r=site/login');
	}

	public function actionDashboard()
	{
		$user = User::model()->findByPk(Yii::app()->user->getId());

		if ($user === NULL) {
			throw new CHttpException(404,'The requested page is no longr valid. Please login again.');
			return;
		}

		$gwinfo = Gateway::model()->find('(gid)=?', array($user->gid));
		if ($gwinfo === null) {
			throw new CHttpException(404,'There is no activated gateway yet.');
			return;
		}

		$this->render('Dashboard', array('gw'=>$gwinfo, 'user'=>$user));
	}

	public function actionAdmindash()
	{
		$user = User::model()->findByPk(Yii::app()->user->getId());

		if ($user === NULL) {
			throw new CHttpException(404,'The requested page is no longr valid. Please login again.');
			return;
		}

		$gwinfo = Gateway::model()->findAllBySql("select * from tbl_gateway;");
		if ($gwinfo === null) {
			throw new CHttpException(404,'There is no activated gateway yet.');
			return;
		}

		$this->render('Admindash', array('gwlist'=>$gwinfo));
	}

	public function actionAdmintest()
	{
		$user = User::model()->findByPk(Yii::app()->user->getId());

		if ($user === NULL) {
			throw new CHttpException(404,'The requested page is no longr valid. Please login again.');
			return;
		}

		$gid = Yii::app()->getRequest()->getParam('gid');

		$gwinfo = Gateway::model()->find('(gid)=?', array($gid));
		$this->render('Admintest', array('gw'=>$gwinfo));
	}

	public function actionDashboardht()
	{
		$user = User::model()->findByPk(Yii::app()->user->getId());

		if ($user === NULL) {
			throw new CHttpException(404,'The requested page is no longr valid. Please login again.');
			return;
		}

		$gwinfo = Gateway::model()->find('(gid)=?', array($user->gid));
		if ($gwinfo === null) {
			throw new CHttpException(404,'There is no activated gateway yet.');
			return;
		}

		$this->render('Dashboardht', array('gw'=>$gwinfo, 'user'=>$user));
	}

	public function actionReports()
	{
		$user = User::model()->findByPk(Yii::app()->user->getId());
		if ($user === NULL) {
			throw new CHttpException(404,'The requested page is no longr valid. Please login again.');
			return;
		}

		$gwinfo = Gateway::model()->find('(gid)=?', array($user->gid));
		if ($gwinfo === null) {
			throw new CHttpException(404,'There is no activated gateway yet.');
			return;
		}

		$this->render('Reportht', array('gw'=>$gwinfo, 'user'=>$user));
	}

	public function actionSettings()
	{
		$user = User::model()->findByPk(Yii::app()->user->getId());
		if ($user === NULL) {
			throw new CHttpException(404,'The requested page is no longr valid. Please login again.');
			return;
		}

		$gwinfo = Gateway::model()->find('(gid)=?', array($user->gid));
		if ($gwinfo === null) {
			throw new CHttpException(404,'There is no activated gateway yet.');
			return;
		}

		$this->render('settings', array('gw'=>$gwinfo, 'user'=>$user));
	}
       
       
      

        

   
}
