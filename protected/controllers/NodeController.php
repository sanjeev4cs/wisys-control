<?php

class NodeController extends MyController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update', 'command'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Node;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Node']))
		{
			$model->attributes=$_POST['Node'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->nid));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Node']))
		{
			$model->attributes=$_POST['Node'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->nid));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Node');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Node('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Node']))
			$model->attributes=$_GET['Node'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Node the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Node::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Node $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='node-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	protected function doCmdToggle($gwinfo, $user)
	{
		$nodes = $gwinfo->searchNodes();
		$chall = $_GET['chall'];
		$cmd = "TOGGLE";

		if ($chall === "on") {
			if ($nodes == NULL) {
				Yii::app()->user->setFlash('message','Error: No nodes associated with this gateway');
				return;
			}

			$params = ':';
			foreach($nodes as $node)
				$params .= $node->nid.":";
		} else {
			$params = ':';
			$cnt = 0;
			foreach($nodes as $node) {
				if ($_GET['c_'.$node->nid] === "on") {
					Yii::log("$node->nid = ON", "info", "controller.site.NodeController");
					$params .= $node->nid.":";
					$cnt++;
				}
				else
					Yii::log("$node->nid = OFF", "info", "controller.site.NodeController");
			}

			if ($cnt === 0) {
				Yii::log("ERROR: No node selected", "info", "controller.site.NodeController");
				Yii::app()->user->setFlash('message','Error: Please select atleast one node');
				$this->redirect('index.php?r=site/dashboard', array('gw'=>$gwinfo));
			}
		}

		Yii::log("COMMAND: $cmd: PARAMS: $params", "info", "controller.site.NodeController");
		Yii::log("Gateway ID = $user->gid", "info", "controller.site.NodeController");

		// Write into command DB
		$data = new Command;
		$data->gid = $gwinfo->id;
		$data->command = $cmd;
		$data->params = $params;
		$data->status = "NEW";
		$data->scode = "1";
		$data->save();

		Yii::log("Command ID = $data->id", "info", "controller.site.NodeController");

		Yii::app()->user->setFlash('message','Success: Command Submitted. Please wait for completion');
	}

	protected function doCmdDim($gwinfo, $user)
	{
		$nodes = $gwinfo->searchNodes();
		$chall = $_GET['chall'];
		$cmd = "DIM";

		if ($chall === "on") {
			if ($nodes == NULL) {
				Yii::app()->user->setFlash('message','Error: No nodes associated with this gateway');
				return;
			}

			$pwm = $_GET['dim_all'];
			$params = ':';

			foreach($nodes as $node)
				$params .= $node->nid."+".$pwm.":";
		} else {
			$params = ':';
			$cnt = 0;
			foreach($nodes as $node) {
				if ($_GET['c_'.$node->nid] === "on") {
					Yii::log("$node->nid = ON", "info", "controller.site.NodeController");
					$pwm = $_GET['r_'.$node->nid];
					$params .= $node->nid."+".$pwm.":";
					$cnt++;
				}
				else
					Yii::log("$node->nid = OFF", "info", "controller.site.NodeController");
			}

			if ($cnt === 0) {
				Yii::log("ERROR: No node selected", "info", "controller.site.NodeController");
				Yii::app()->user->setFlash('message','Error: Please select atleast one node');
				$this->redirect('index.php?r=site/dashboard', array('gw'=>$gwinfo));
			}
		}

		Yii::log("COMMAND: $cmd: PARAMS: $params", "info", "controller.site.NodeController");
		Yii::log("Gateway ID = $user->gid", "info", "controller.site.NodeController");

		// Write into command DB
		$data = new Command;
		$data->gid = $gwinfo->id;
		$data->command = $cmd;
		$data->params = $params;
		$data->status = "NEW";
		$data->scode = "1";
		$data->save();

		Yii::log("Command ID = $data->id", "info", "controller.site.NodeController");

		Yii::app()->user->setFlash('message','Success: Command Submitted. Please wait for completion');
	}

	protected function doSchedule($gwinfo, $user)
	{
		Yii::log("Executing schedule command", "info", "controller.site.NodeController");

		$schedule = $gwinfo->searchSchedules();
		$cnt = 0;
		foreach($schedule as $s) {
			if ($s->status === "PENDING")
				$cnt++;
		}

		if ($cnt == 0) {
			Yii::app()->user->setFlash('message','Error: All schedules are already applied at the Gateway');
			$this->redirect('index.php?r=schedule/index');
		}

		// Write into command DB
		$data = new Command;
		$data->gid = $gwinfo->id;
		$data->command = "SCHEDULE";
		$data->params = "ALL";
		$data->status = "NEW";
		$data->scode = "1";
		$data->save();

		Yii::log("Command ID = $data->id", "info", "controller.site.NodeController");

		Yii::app()->user->setFlash('message','Success: Command Submitted. Please wait for completion');
		$this->redirect('index.php?r=schedule/index');
	}

	public function actionCommand()
	{
		$user = User::model()->findByPk(Yii::app()->user->getId());
		$gwinfo = Gateway::model()->find('(gid)=?', array($user->gid));

		if(isset($_GET['reset']))
			$this->redirect('index.php?r=site/dashboard', array('gw'=>$gwinfo, 'user'=>$user));

		if ($gwinfo->g_alive == 0) {
			Yii::app()->user->setFlash('message','Error: Link is not active. Please contact maintenance');
			$this->redirect('index.php?r=site/dashboard', array('gw'=>$gwinfo, 'user'=>$user));
		}

		// Check that there is no command in progress for this gayeway
		$cmd = Command::model()->find('(gid)=?', array($gwinfo->id));
		if ($cmd) {
			// check the last command scode
			if ($cmd->scode == 1) {
				// Previous command in progress
				Yii::log("ERROR: Previous command in progress for gwid = $user->gid", "info", "controller.site.NodeController");
				Yii::app()->user->setFlash('message','Error: GW '.$user->gid.' , Please wait for previous command to finish');
				$this->redirect('index.php?r=site/dashboard', array('gw'=>$gwinfo, 'user'=>$user));
			}

			// Delete the previous comamnd entry as it is completed
			$cmd->delete();
		} else
			Yii::log("cannot find record", "info", "controller.site.NodeController");

		if(isset($_GET['onoff'])) {
			$this->doCmdToggle($gwinfo, $user);
		}

		if(isset($_GET['dim'])) {
			$this->doCmdDim($gwinfo, $user);
		}

		if(isset($_GET['schedule'])) {
			$this->doSchedule($gwinfo, $user);
		}

		$this->redirect('index.php?r=site/dashboard', array('gw'=>$gwinfo, 'user'=>$user));
	}
}
