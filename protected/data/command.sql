CREATE TABLE tbl_command(id integer primary key autoincrement, gid integer, command text, params text, status text, scode int);
CREATE TABLE tbl_user(id integer primary key autoincrement, username varchar(128) not null, password char(64) not null, email varchar(128) not null, gid varchar(18) not null, location varchar(128), app varchar(64));
CREATE TABLE tbl_schedule(sid integer primary key autoincrement, gid integer, type text, time varchar(6), pwm integer, status varchar(10));
PRAGMA journal_mode=WAL;
