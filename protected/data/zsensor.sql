CREATE TABLE tbl_sensor(id integer primary key autoincrement, gid integer, nid integer, stype integer, sdata integer, timestamp DATE DEFAULT (datetime('now', 'utc')));
CREATE TABLE tbl_gateway(id integer primary key autoincrement, gid varchar(18), vid varchar(12), node_cnt int, z_alive int, g_alive int);
CREATE TABLE tbl_node("nid" integer PRIMARY KEY autoincrement, "gid" integer,"src_addr" smallint,"nw_addr" smallint,"ieee_addr" varchar(24), "capab" smallint, "status" tinyint,"sensors" integer DEFAULT (null), "lqi" integer, "pktloss" integer, "interval" smallint);
PRAGMA journal_mode=WAL;
